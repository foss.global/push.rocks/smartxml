import { expect, tap } from '@pushrocks/tapbundle';
import * as smartxml from '../ts/index';

let testSmartxml: smartxml.SmartXml;
let testXml = `
  <hello>
    <wow>nice</wow>
  </hello>
`;

tap.test('should create an instance', async () => {
  testSmartxml = new smartxml.SmartXml();
});

tap.test('should create an xml string', async () => {
  const xmlResult = testSmartxml.createXmlFromObject({
    hello: {
      "@_xlmns:teststring": "hellothere",
      "@_xlmns:testnumber": 10,
      wow: 'test',
      url: [{loc: 3},{loc: 3}]
    }
  });
  console.log(xmlResult);
});

tap.test('should parse an yml file', async () => {
  const jsObject = testSmartxml.parseXmlToObject(testXml);
  console.log(jsObject);
  expect(typeof jsObject).to.equal('object');
  expect(jsObject).property('hello').property('wow').to.equal('nice');
});


tap.start();
