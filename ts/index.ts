import * as plugins from './smartxml.plugins';

export class SmartXml {
  constructor() {}

  public parseXmlToObject<T = any>(xmlStringArg: string): T {
    const jsonObject = plugins.fastXmlParser.parse(xmlStringArg);
    return jsonObject;
  }

  public createXmlFromObject(jsObject: any): string {
    const jsToXmlParser = new plugins.fastXmlParser.j2xParser({
      ignoreAttributes: false,
      attributeNamePrefix: "@_",
      format: true,
      indentBy: '  ',
    });
    const xml = jsToXmlParser.parse(jsObject);
    return '<?xml version="1.0" encoding="UTF-8"?>\n' + xml;
  }
}